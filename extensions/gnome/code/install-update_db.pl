
use Bugzilla;
use Bugzilla::Constants;
use Bugzilla::Field;
use Bugzilla::Product;

my $dbh = Bugzilla->dbh;

# Convert the flags tables into a simple attachment status_id field
if (!$dbh->bz_column_info('attachments', 'status_id')) {
    print "Converting flags tables into a simple attachment status field...\n";

    # Add a status_id column to the attachments table
    $dbh->bz_add_column('attachments', 'status_id',
                        {TYPE => 'INT2', NOTNULL => 1}, 0);

    # Make room for a flagtype with id 0 with name 'none' (we want 0 because we
    # just barely defined it to be the default for attachments.status_id)
    $sth = $dbh->prepare("SELECT id FROM flagtypes where name = 'none'");
    $sth->execute;
    unless ($sth->rows) {
        $sth = $dbh->prepare("SELECT MAX(id) + 1 FROM flagtypes");
        $sth->execute();
        my ($max_id) = $sth->fetchrow_array();
        $dbh->do("UPDATE flagtypes SET id = ? WHERE id = 0", undef, ($max_id));
        $dbh->do("UPDATE flags SET type_id = ? WHERE type_id = 0", undef, ($max_id));
        $dbh->do("UPDATE flagexclusions SET type_id = ? WHERE type_id = 0", undef, ($max_id));
        $dbh->do("UPDATE flaginclusions SET type_id = ? WHERE type_id = 0", undef, ($max_id));

        # Make the flagtype 'none'
        $dbh->do("INSERT INTO flagtypes (id, name, description, target_type)
                  VALUES (0, 'none', 'an unreviewed patch', 'a')");
    }

    # Now, set the actual status for bugs that already had one, using the flags
    # table.
    $sth = $dbh->prepare("SELECT attachments.attach_id, type_id
                          FROM attachments
                          INNER JOIN flags
                          ON attachments.attach_id = flags.attach_id
                          INNER JOIN flagtypes
                          ON flags.type_id = flagtypes.id
                          WHERE ispatch = 1
                          AND type_id IS NOT NULL
                          ORDER BY attach_id, sortkey DESC");
    $sth->execute();
    while (my ($attach, $status_id) = $sth->fetchrow_array()) {
        $dbh->do("UPDATE attachments SET status_id = ? WHERE attach_id = ?",
                 undef, ($status_id, $attach));
    }

    # Now, update the bugs_activity table to get rid of all those
    # stinkin' +'s in the added and removed columns
    $dbh->do("UPDATE bugs_activity
              SET removed = replace(removed, '+', ''),
                  added = replace(added, '+', '')
              WHERE fieldid=74");

    print "done.\n";
}

# Move stuff into custom fields
my $gnome_version = new Bugzilla::Field({'name' => 'cf_gnome_version'});
if (!$gnome_version) {
    $gnome_version = Bugzilla::Field->create({
        name        => 'cf_gnome_version',
        description => 'GNOME version',
        type        => FIELD_TYPE_SINGLE_SELECT,
        sortkey     => 200,
        mailhead    => 1,
        enter_bug   => 1,
        obsolete    => 0,
        custom      => 1,
    });
}

my $gnome_target = new Bugzilla::Field({'name' => 'cf_gnome_target'});
if (!$gnome_target) {
    $gnome_target = Bugzilla::Field->create({
        name        => 'cf_gnome_target',
        description => 'GNOME target',
        type        => FIELD_TYPE_SINGLE_SELECT,
        sortkey     => 200,
        mailhead    => 1,
        enter_bug   => 1,
        obsolete    => 0,
        custom      => 1,
    });
}

# Port the old field to the new custom field
sub port_field {
    my ($old_field_name, $new_field_name) = (@_);

    my $old_field = new Bugzilla::Field({'name' => $old_field_name});
    my $new_field = new Bugzilla::Field({'name' => $new_field_name});

    if ($old_field && $new_field) {
        $dbh->do('UPDATE bugs_activity SET fieldid = ? WHERE fieldid = ?', undef, 
                 ($new_field->id, $old_field->id));

        # Move options
        $dbh->do("INSERT INTO $new_field_name SELECT * FROM $old_field_name WHERE id > 1");

        my $new_default_value = $dbh->selectrow_array(
            "SELECT value FROM $new_field_name WHERE id = ?", undef, 1);
        my $old_default_value = $dbh->selectrow_array(
            "SELECT value FROM $old_field_name WHERE id = ?", undef, 1);

        $dbh->do("UPDATE bugs SET $new_field_name = $old_field_name");
        $dbh->do("UPDATE bugs SET $new_field_name = ? WHERE $new_field_name = ?",
            undef, ($new_default_value, $old_default_value));

        $dbh->bz_drop_column('bugs', $old_field_name);
        $dbh->do('DELETE FROM fielddefs WHERE id = ?', undef,
                 $old_field->id);
        $dbh->bz_drop_table($old_field_name);
    }
}

port_field('gnome_version', 'cf_gnome_version');
port_field('gnome_target', 'cf_gnome_target');

# Unused columns
$dbh->bz_drop_column('profiles', 'is_top_bug_reporter');
$dbh->bz_drop_column('profiles', 'is_top_bug_closer');

my $hackers = new Bugzilla::Group({'name' => 'hackers'});
my $canconfirm = new Bugzilla::Group({'name' => 'canconfirm'});
my $editbugs = new Bugzilla::Group({'name' => 'editbugs'});
my $editcomponents = new Bugzilla::Group({'name' => 'editcomponents'});

if ($hackers) {
    my $sth_ggm = $dbh->prepare('INSERT INTO group_group_map
                                (member_id, grantor_id, grant_type)
                                VALUES (?, ?, ?)');

    my $sth_gcm = $dbh->prepare('INSERT INTO group_control_map
                                 (group_id, membercontrol, othercontrol,
                                  product_id, editcomponents)
                                 VALUES (?, ?, ?, ?, ?)');

    my $sth_ugm = $dbh->prepare('INSERT INTO user_group_map
                                 (grant_type, group_id, isbless, user_id)
                                 VALUES (?, ?, ?, ?)');

    my $sth_ugm_drop = $dbh->prepare('DELETE FROM user_group_map WHERE
                                      user_id = ? AND group_id = ?
                                      AND grant_type = ?');

    # Ensure people within the hackers group inherit editbugs,canconfirm
    $dbh->do("DELETE FROM group_group_map
              WHERE member_id = ?
                AND grantor_id IN (?, ?)
                AND grant_type IN (?, ?)", undef,
             ($hackers->id, $canconfirm->id, $editbugs->id, GROUP_MEMBERSHIP,
              GROUP_VISIBLE));

    $sth_ggm->execute($hackers->id, $canconfirm->id, GROUP_MEMBERSHIP);
    $sth_ggm->execute($hackers->id, $canconfirm->id, GROUP_VISIBLE);

    $sth_ggm->execute($hackers->id, $editbugs->id, GROUP_MEMBERSHIP);
    $sth_ggm->execute($hackers->id, $editbugs->id, GROUP_VISIBLE);

    # Create product specific groups:
    my $products = $dbh->selectcol_arrayref('SELECT id FROM products');
    foreach my $product (@{ Bugzilla::Product->new_from_list($products)} ) {
        my $group = new Bugzilla::Group({'name' => $product->name});
        if (!$group) {
            # Create the group
            $group = Bugzilla::Group->create({
                name        => $product->name,
                description => $product->name . " developers",
                isbuggroup  => 1,
            });

            # Ensure members of this newly created group inherit the hackers group automatically
            $sth_ggm->execute($group->id, $hackers->id, GROUP_MEMBERSHIP);
            $sth_ggm->execute($group->id, $hackers->id, GROUP_VISIBLE); # XXX not sure what this does

            # Ensure bugs can be restricted to this group and also developers
            # have editcomponents just for this product
            $sth_gcm->execute($group->id, CONTROLMAPSHOWN, CONTROLMAPNA,
                              $product->id, 1);

        }
        
        # Make existing 'developers' members of this group
        my $devs = $dbh->selectcol_arrayref('SELECT userid FROM developers
                                            WHERE product_id = ?', undef,
                                            $product->id);
        foreach my $dev (@$devs) {
            $sth_ugm_drop->execute($dev, $group->id, GRANT_DIRECT);
            $sth_ugm->execute(GRANT_DIRECT, $group->id, 0, $dev);
            $sth_ugm_drop->execute($dev, $editcomponents->id, GRANT_DIRECT);
        }
    }
    
    # Drop developers table as all developers have been migrated by now
    $dbh->bz_drop_table('developers');
    # Rename it to GNOME developers
    $dbh->do('UPDATE groups SET name = ?, description = ? WHERE name = ?',
             undef, ('developers', 'GNOME developers', 'hackers'));
}

# Convert the points from GNOME Bugzilla 2.20 to the new format
if (!$dbh->bz_column_info('profiles', 'add_comments')
    ||$dbh->bz_column_info('profiles', 'points')) {
    $dbh->bz_add_column('profiles', 'bugs_reported',
        {TYPE => 'INT2', NOTNULL => 1, DEFAULT => 0});
    $dbh->bz_add_column('profiles', 'bugs_closed',
        {TYPE => 'INT2', NOTNULL => 1, DEFAULT => 0});
    $dbh->bz_add_column('profiles', 'add_comments',
        {TYPE => 'INT2', NOTNULL => 1, DEFAULT => 0});

    # Prepare a bunch of queries for each user we need to update
    # XXX - this is inefficient -- should use single queries instead
    my $sth_get_comment_count =
      $dbh->prepare("SELECT COUNT(thetext) FROM longdescs WHERE who = ?");
    my $sth_get_bugs_closed =
      $dbh->prepare("SELECT COUNT(bugs.bug_id)
                      FROM bugs
                     INNER JOIN bugs_activity ON bugs.bug_id = bugs_activity.bug_id
                     WHERE bugs.bug_status IN ('RESOLVED','CLOSED','VERIFIED')
                       AND bugs_activity.added IN ('RESOLVED','CLOSED')
                       AND bugs_activity.bug_when =
                             (SELECT MAX(bug_when)
                                FROM bugs_activity ba
                               WHERE ba.added IN ('RESOLVED','CLOSED')
                                 AND ba.removed IN ('UNCONFIRMED','REOPENED',
                                                    'NEW','ASSIGNED','NEEDINFO')
                                 AND ba.bug_id = bugs_activity.bug_id)
                       AND bugs_activity.who = ?");
    my $sth_get_bugs_reported =
      $dbh->prepare("SELECT COUNT(DISTINCT bug_id)
                     FROM bugs
                     WHERE bugs.reporter = ?
                       AND NOT (bugs.bug_status = 'RESOLVED' AND 
                                bugs.resolution IN ('DUPLICATE','INVALID','NOTABUG',
                                                    'NOTGNOME','INCOMPLETE'))");

    my $sth_update_stats = 
        $dbh->prepare("UPDATE profiles SET add_comments = ?, bugs_closed = ?, 
                       bugs_reported = ? WHERE userid = ?");

    $users = $dbh->selectcol_arrayref(
        "SELECT userid FROM profiles WHERE bugs_reported = 0 AND bugs_closed = 0
                                           AND add_comments = 0");
    
    foreach my $userid (@$users) {
        $sth_get_comment_count->execute($userid);
        my $comment_count = $sth_get_comment_count->fetchrow_array;

        $sth_get_bugs_closed->execute($userid);
        my $bugs_closed = $sth_get_bugs_closed->fetchrow_array;

        $sth_get_bugs_reported->execute($userid);
        my $bugs_reported = $sth_get_bugs_reported->fetchrow_array;

        $sth_update_stats->execute($comment_count, $bugs_closed,
                                   $bugs_reported, $userid);
    }

    $dbh->bz_drop_column('profiles', 'points');
}

1;
